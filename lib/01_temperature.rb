def ftoc(ftemp)
  ((ftemp-32)*(5.0/9.0)).to_i
end

def ctof(ctemp)
  (ctemp.to_f/(5.0/9.0)+32)
end

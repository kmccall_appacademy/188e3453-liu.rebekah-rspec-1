def first_consonants(word)
  consonants = ""
  word_array = word.chars
  word_array.each_with_index do |letter, ind|
    if "aeioAEIO".include?(letter)
      break
    elsif "uU".include?(letter) && "qQ".include?(word_array[ind-1])
      consonants << letter
    else
      consonants << letter
    end
  end
  consonants
end

def translate(phrase)
  translated = []
  phrase.split.each do |word|
    translated << word[first_consonants(word).length..-1]+first_consonants(word)+"ay"
  end
  translated.join(" ")
end

def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  sum = 0
  array.each {|num| sum = sum + num}
  sum
end

def multiply (array)
  answer = 1
  array.each {|num| answer = answer * num}
  answer
end

def power (num1, num2)
  num1 ** num2
end

def factorial(num)
  factorial = 1
  (1..num).to_a.each {|int| factorial = factorial * int}
  factorial
end

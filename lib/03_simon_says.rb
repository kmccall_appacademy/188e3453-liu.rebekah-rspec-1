def echo(phrase)
  phrase
end

def shout(phrase)
  phrase.upcase
end

def repeat(phrase,times = 2)
  repeat_array(phrase, times).join(" ")
end

def repeat_array(phrase, times)
  array = []
  (1..times).each {|num| array[num-1] = phrase}
  array
end

def start_of_word(phrase,number)
  phrase[0..number-1]
end

def first_word(phrase)
  phrase.split.first
end

def titleize(phrase)
  def capitalize_each_word(phrase)
    array = []
    phrase.split.each do |word|
      if "and over the".include?(word)
        array << word
      else
        array << word.capitalize
      end
    end
    array[0] = array[0].capitalize
    array
  end
  capitalize_each_word(phrase).join(" ")
end
